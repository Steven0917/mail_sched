from flask import Flask
from flask_apscheduler import APScheduler
import logging
from logging.handlers import RotatingFileHandler

class Config(object):
    JOBS = [
        {
            'id': 'job_report',
            'func': 'jobs.report:send_report',
            'args': (9527, 'tyx.jpg'),
            'trigger': 'interval',
            'seconds': 10
            # 'minutes': 1
        }
    ]

    SCHEDULER_API_ENABLED = True


if __name__ == '__main__':
    app = Flask(__name__)

    handler = RotatingFileHandler('jobs.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    app.config.from_object(Config())
    scheduler = APScheduler()
    # it is also possible to enable the API directly
    # scheduler.api_enabled = True
    scheduler.init_app(app)
    scheduler.start()

    app.run(debug=True)
