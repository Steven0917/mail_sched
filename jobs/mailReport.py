#  coding:utf-8

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

class MailReport(object):

    def __init__(self):
        self.smtpHost = 'smtp.gmail.com'
        self.smtpPort = '25'
        self.username = 'ctstest9527@gmail.com'
        self.password = 'test9527'
        self.rcptlist = ['ctstest9527@gmail.com', 'ctstest9527@gmail.com']

        self.reportId = 9527
        self.pdf = 'tyx.jpg'

    def sendReport(self):
        # Build multipart mail message
        msg = MIMEMultipart('mixed')
        msg['Subject'] = 'Daily Report ' + str(self.reportId)
        msg['From'] = self.username
        msg['To'] = ','.join(self.rcptlist)

        # Build text/plain part of multipart/alternative
        alternative = MIMEMultipart('alternative')
        textplain = MIMEText('Daily Report ' + str(self.reportId))

        alternative.attach(textplain)

        # Add alternative into mixed
        msg.attach(alternative)

        # Attachment type
        # xlsx type attachment
        # xlsxpart = MIMEApplication(open('1.xlsx', 'rb').read())
        # xlsxpart.add_header('Content-Disposition', 'attachment', filename='1.xlsx')
        # msg.attach(xlsxpart)

        # jpg type attachment
        jpgpart = MIMEApplication(open(self.pdf, 'rb').read())
        jpgpart.add_header('Content-Disposition', 'attachment', filename=self.pdf)
        msg.attach(jpgpart)

        try:
            # client = smtplib.SMTP()
            # SSL may be needed to create a client in python 2.7 or later
            #client = smtplib.SMTP_SSL()
            # client.connect('smtpdm.aliyun.com')
            # client.login(username, password)
            #

            s = smtplib.SMTP(self.smtpHost, self.smtpPort)
            s.set_debuglevel(True)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(self.username, self.password)

            # Sender has to match the authorized address
            s.sendmail(self.username, self.rcptlist, msg.as_string())
            # client.quit()
            s.close()
            print 'Email delivered successfully!'
        except smtplib.SMTPRecipientsRefused:
            print 'Email delivery failed, invalid recipient'
        except smtplib.SMTPAuthenticationError:
            print 'Email delivery failed, authorization error'
        except smtplib.SMTPSenderRefused:
            print 'Email delivery failed, invalid sender'
        except smtplib.SMTPException as e:
            print 'Email delivery failed, ', e.message


if __name__ == '__main__':
    report = MailReport()
    report.sendReport()