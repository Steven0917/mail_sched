
from jobs.mailReport import MailReport

class Report(MailReport):
    def __init__(self, id, pdf):
        super(Report, self).__init__()
        self.reportId = id
        self.pdf = pdf

def send_report(id, pdf):
    print id, pdf

    report = Report(id, pdf)
    report.sendReport()


if __name__ == '__main__':
    send_report(9527, 'tyx.jpg')