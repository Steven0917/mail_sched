# -*- coding:utf-8 -*-
import urllib, urllib2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication


smtpHost = 'smtp.gmail.com'
smtpPort = '25'
sslPort  = '587'
fromMail = 'ctstest9527@gmail.com'
toMail   = 'tengxin1981@yahoo.com'
username = 'ctstest9527'
password = 'test9527'

# Sender address, created in the console
username = 'ctstest9527@gmail.com'
# Sender password, created in the console
password = 'test9527'

# Recipient address list, supports up to 30 recipients
rcptlist = ['ctstest9527@gmail.com', 'tengxin1981@yahoo.com']
receivers = ','.join(rcptlist)

# Build multipart mail message
msg = MIMEMultipart('mixed')
msg['Subject'] = 'Test Email'
msg['From'] = username
msg['To'] = receivers

# Build text/plain part of multipart/alternative
alternative = MIMEMultipart('alternative')
textplain = MIMEText ('plain text part')
alternative.attach(textplain)


# Add alternative into mixed
msg.attach(alternative)

# Attachment type
# xlsx type attachment
# xlsxpart = MIMEApplication(open('1.xlsx', 'rb').read())
# xlsxpart.add_header('Content-Disposition', 'attachment', filename='1.xlsx')
# msg.attach(xlsxpart)

# jpg type attachment
jpgpart = MIMEApplication(open('tyx.jpg', 'rb').read())
jpgpart.add_header('Content-Disposition', 'attachment', filename='tyx.jpg')
msg.attach(jpgpart)

# mp3 type attachment
# mp3part = MIMEApplication(open('3.mp3', 'rb').read())
# mp3part.add_header('Content-Disposition', 'attachment', filename='3.mp3')
# msg.attach(mp3part)
# Send mail

try:
    # client = smtplib.SMTP()
    # SSL may be needed to create a client in python 2.7 or later
    #client = smtplib.SMTP_SSL()
    # client.connect('smtpdm.aliyun.com')
    # client.login(username, password)
    #

    s = smtplib.SMTP(smtpHost, smtpPort)
    s.set_debuglevel(True)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(username, password)

    # Sender has to match the authorized address
    s.sendmail(username, rcptlist, msg.as_string())
    # client.quit()
    s.close()
    print 'Email delivered successfully!'
except smtplib.SMTPRecipientsRefused:
    print 'Email delivery failed, invalid recipient'
except smtplib.SMTPAuthenticationError:
    print 'Email delivery failed, authorization error'
except smtplib.SMTPSenderRefused:
    print 'Email delivery failed, invalid sender'
except smtplib.SMTPException as e:
    print 'Email delivery failed, ', e.message